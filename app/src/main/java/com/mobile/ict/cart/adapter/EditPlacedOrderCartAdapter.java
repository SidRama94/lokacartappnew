package com.mobile.ict.cart.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.Master;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vish on 22/4/16.
 */
public class EditPlacedOrderCartAdapter extends RecyclerView.Adapter<EditPlacedOrderCartAdapter.DataObjectHolder> {
    //public static ArrayList<Product> itemList = new ArrayList<>();
    private Context context;
    DataObjectHolder rcv;
    TextView cartTotal;
    double sum=0.0;
    Bundle b;
    HashMap<String,Integer> itemsStockQuantityHashMap = new HashMap<>();
    String stockEnabledStatus;
    HashMap<String,Integer> itemsQuantityHashMap = new HashMap<String, Integer>();
   // public  ArrayList<Integer> changeCheck;
    boolean fromTextWatchr = false;

    /*public  ArrayList<Integer> getChangeCheck()
    {
        return changeCheck;
    }*/
    public EditPlacedOrderCartAdapter(Context context,TextView cartTotal,Bundle bundle,HashMap<String,Integer> itemsStockQuantityHashMap,String stockEnabledStatus)
    {
       // this.itemList = itemList;
        this.context = context;
        this.cartTotal=cartTotal;
     //   this.b = bundle;
        this.stockEnabledStatus=stockEnabledStatus;
        this.itemsStockQuantityHashMap = itemsStockQuantityHashMap;
        //Master.changeCheckList = new ArrayList<>();
        System.out.println("total sum---------------------"+cartTotal.getText().toString().trim());

    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView tProductName, tPrice, tAvailable, tItemTotal,tStockStatus;
        ImageButton  bPlus, bMinus;
        EditText eQuantity;


        MyCustomEditTextListener myCustomEditTextListener;

        public DataObjectHolder(final View itemView, final Context context,MyCustomEditTextListener myCustomEditTextListener)
        {
            super(itemView);
            tProductName = (TextView) itemView.findViewById(R.id.tCartProductName);
            tPrice = (TextView) itemView.findViewById(R.id.tPrice);
            tItemTotal = (TextView) itemView.findViewById(R.id.tItemTotal);
            tStockStatus = (TextView) itemView.findViewById(R.id.tStockStatus);
            bPlus = (ImageButton) itemView.findViewById(R.id.bPlus);
            bMinus = (ImageButton) itemView.findViewById(R.id.bMinus);
            eQuantity = (EditText) itemView.findViewById(R.id.eQuantity);
            this.myCustomEditTextListener = myCustomEditTextListener;
        }


    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_edit_cart, parent, false);

        rcv = new DataObjectHolder(cardView, context,new MyCustomEditTextListener());

        return rcv;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {


        holder.eQuantity.addTextChangedListener(holder.myCustomEditTextListener);
        holder.myCustomEditTextListener.updatePosition(position, holder.tItemTotal, holder.eQuantity,
                holder.bPlus, holder.bMinus, cartTotal, holder.tStockStatus);
        holder.tProductName.setText("" + Master.editOrderList.get(position).getName());
        holder.tPrice.setText("\u20B9" + Master.editOrderList.get(position).getUnitPrice());

        HashMap<String, Integer> sortList = new HashMap<String, Integer>(itemsStockQuantityHashMap);
      /*  int quantity;
        if(itemsQuantityHashMap.containsKey(Master.editOrderList.get(position).getName())){
            quantity = itemsQuantityHashMap.get( Master.editOrderList.get(position).getName());
        }
        else if(b.containsKey( Master.editOrderList.get(position).getName())){
            quantity = b.getInt(Master.editOrderList.get(position).getName(), 1);
        }
        else{
            quantity = 1;
            itemsQuantityHashMap.put(Master.editOrderList.get(position).getName(), 1);
        }

        changeCheck.add(quantity);


        holder.eQuantity.setText(quantity + "");

        Master.editOrderList.get(position).setTotal(Double.parseDouble(String.format("%.2f", quantity * Master.editOrderList.get(position).getUnitPrice())));
*/


      //  changeCheck.add(Master.editOrderList.get(position).getQuantity());


        System.out.println("111111111111-------name: " + Master.editOrderList.get(position).getName() + " Quantity: " +  Master.editOrderList.get(position).getQuantity() + " Stock Quantity: " +  Master.editOrderList.get(position).getStockQuantity());
        //holder.eQuantity.setText("" + Master.editOrderList.get(position).getQuantity());
        holder.tItemTotal.setText("\u20B9" + Master.editOrderList.get(position).getTotal());

        if(Master.editOrderList.get(position).getQuantity() < 1)
        {
            Log.e("Cart adapter", "quant <= 1");
            holder.bMinus.setEnabled(false);
            holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
        }

        holder.bPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sqty = holder.eQuantity.getText().toString().trim();
                int dqty;
                if(sqty.equals(""))
                    dqty = 1;
                else
                    dqty = Integer.parseInt(sqty) + 1;

                //System.out.println("adding-------" + Master.editOrderList.get(position).getName() + "--------" + dqty);

                //  holder.eQuantity.setText("" + dqty);

                if(dqty >= 999)
                {
                    holder.eQuantity.setText("999");
                    holder.bPlus.setEnabled(false);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                    Log.e("cart adapter", "in 999 if");
                }
                else if(dqty >= Master.changeCheckList.get(position) && holder.tStockStatus != null &&
                        holder.tStockStatus.getVisibility() == View.VISIBLE &&
                        holder.tStockStatus.getText().toString().equals("OUT OF STOCK"))
                {
                    holder.bPlus.setEnabled(false);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                    holder.eQuantity.setText("" + Master.changeCheckList.get(position));
                    Log.e("cart adapter", "in changeCheck if");
                }
                else
                {
                    holder.bPlus.setEnabled(true);
                    holder.bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                    holder.eQuantity.setText("" + dqty);
                    Log.e("cart adapter", "else");
                }

                holder.bMinus.setEnabled(true);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
            }
        });

        holder.bMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sqty = holder.eQuantity.getText().toString().trim();

                int dqty = Integer.parseInt(sqty);

                if(dqty > 1)
                    holder.eQuantity.setText("" + --dqty);

                else if(dqty == 1)
                {
                    Log.e("Cart adapter", "in ocClick quant <= 1");
                    holder.eQuantity.setText("");
                    holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                    holder.bMinus.setEnabled(false);
                }

            }
        });




        if(stockEnabledStatus.equals("true"))
        {
            holder.tStockStatus.setVisibility(View.VISIBLE);
            holder.eQuantity.setEnabled(true);
            if(Master.editOrderList.get(position).getStockQuantity() !=0 )
            {
                holder.tStockStatus.setText("STOCK AVAILABLE");

                holder.bPlus.setEnabled(true);
                holder.bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);


                holder.bMinus.setEnabled(true);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                holder.tStockStatus.setTextColor(Color.parseColor("#42A462"));

                System.out.println("in stock available-------name: " + Master.editOrderList.get(position).getName() + " Quantity: " +  Master.editOrderList.get(position).getQuantity() + " Stock Quantity: " +  Master.editOrderList.get(position).getStockQuantity());

            }
            else
            {

                holder.tStockStatus.setText("OUT OF STOCK");
                holder.bPlus.setEnabled(false);
                holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                holder.tStockStatus.setTextColor(Color.RED);
                System.out.println("in out of stock-------name: " + Master.editOrderList.get(position).getName() + " Quantity: " +  Master.editOrderList.get(position).getQuantity() + " Stock Quantity: " +  Master.editOrderList.get(position).getStockQuantity());
            }


            if(Master.editOrderList.get(position).getQuantity() == 1)
            {
                holder.bMinus.setEnabled(false);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
            }
            else
            {
                holder.bMinus.setEnabled(true);
                holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
            }


/*
            for(HashMap.Entry<String, Integer> entry : sortList.entrySet())
            {
                if(entry.getKey().equals(Master.editOrderList.get(position).getName()))
                {
                    holder.eQuantity.setEnabled(true);
                    if(entry.getValue()!=0)
                    {
                        holder.tStockStatus.setText("STOCK AVAILABLE");

                        holder.bPlus.setEnabled(true);
                        holder.bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);


                        holder.bMinus.setEnabled(true);
                        holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                        holder.tStockStatus.setTextColor(Color.parseColor("#42A462"));
                    }
                    else
                    {
                        holder.tStockStatus.setText("OUT OF STOCK");
                        holder.bPlus.setEnabled(false);
                        holder.bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                        holder.tStockStatus.setTextColor(Color.RED);
                    }

                    if(Integer.parseInt(holder.eQuantity.getText().toString())==1)
                    {
                        holder.bMinus.setEnabled(false);
                        holder.bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                    }
                    else
                    {
                        holder.bMinus.setEnabled(true);
                        holder.bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                    }


                }
            }*/


        }
        else
        {
            holder.tStockStatus.setVisibility(View.GONE);
            holder.tStockStatus.setText("Status");
            holder.eQuantity.setEnabled(true);
        }

        holder.eQuantity.setText("" + Master.editOrderList.get(position).getQuantity());
    }

    @Override
    public int getItemCount() {
        return Master.editOrderList.size();
    }



    public static ArrayList<Product> getList()
    {
        for(int i=0;i<Master.editOrderList.size();i++)
        {
            System.out.println("---------------------");
            System.out.println("Name: " + Master.editOrderList.get(i).getName());
            System.out.println("Qty: " + Master.editOrderList.get(i).getQuantity());
            System.out.println("price: " + Master.editOrderList.get(i).getUnitPrice());
            System.out.println("total: " + Master.editOrderList.get(i).getTotal());
            System.out.println("---------------------");
        }

        return  Master.editOrderList;
    }



    private class MyCustomEditTextListener implements TextWatcher {
        private int position;
        TextView itotal;
        EditText eqty;
        TextView cartTotal, tStockStatus;
        ImageButton bPlus, bMinus;

        public void updatePosition(int position,TextView textView,EditText editText, ImageButton bPlus,
                                   ImageButton bMinus, TextView cartTotal, TextView tStockStatus) {
            this.position = position;
            itotal=textView;
            eqty=editText;
            this.bPlus = bPlus;
            this.bMinus = bMinus;
            this.cartTotal=cartTotal;
            this.tStockStatus = tStockStatus;
        }



        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op

            try
            {
                /*if(!fromTextWatchr)
                {
                    fromTextWatchr = false;*/
                    if (!editable.toString().equals("")) {
                        String total;
                        try {

                            bMinus.setEnabled(true);
                            bMinus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);

                            System.out.println(Master.editOrderList.get(position).getName() +" -------- stock status text: " + tStockStatus.getText().toString() + "------- Quantity: " + Master.editOrderList.get(position).getQuantity() + "------ edittext value: " + editable.toString() + " Stock Quantity: " +  Master.editOrderList.get(position).getStockQuantity());

                            if(Integer.parseInt(editable.toString()) > 999)
                            {
                               // fromTextWatchr = true;
                                eqty.setText("999");
                                bPlus.setEnabled(false);
                                bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                                total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * 999);
                                Master.editOrderList.get(position).setQuantity(999);
                                System.out.println("2222222222-------name: " + Master.editOrderList.get(position).getName() + " Quantity: " +  Master.editOrderList.get(position).getQuantity());
                               // b.putInt(Master.editOrderList.get(position).getName(), 999);
                            }
                            else if(Integer.parseInt(editable.toString()) == 0)
                            {
                                bMinus.setEnabled(false);
                                bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                                bPlus.setEnabled(true);
                                bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                                total= String.format("%.2f",Master.cartList.get(position).getUnitPrice() * 0);
                                Master.editOrderList.get(position).setQuantity(0);
                            }
                            else if(Integer.parseInt(editable.toString()) == Master.changeCheckList.get(position) && tStockStatus != null &&
                                    tStockStatus.getVisibility() == View.VISIBLE && tStockStatus.getText().toString().equals("OUT OF STOCK"))
                            {
                                bPlus.setEnabled(false);
                                bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                                total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * Master.changeCheckList.get(position));
                                Master.editOrderList.get(position).setQuantity(Master.changeCheckList.get(position));
                                System.out.println("33333333-------name: " + Master.editOrderList.get(position).getName() + " Quantity: " +  Master.editOrderList.get(position).getQuantity());
                            }
                            else if(Integer.parseInt(editable.toString()) > Master.changeCheckList.get(position) && tStockStatus != null &&
                                    tStockStatus.getVisibility() == View.VISIBLE && tStockStatus.getText().toString().equals("OUT OF STOCK"))
                            {
                                bPlus.setEnabled(false);
                                bPlus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);
                                eqty.setText("" + Master.changeCheckList.get(position));
                                total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * Master.changeCheckList.get(position));
                                Master.editOrderList.get(position).setQuantity(Master.changeCheckList.get(position));
                                System.out.println("444444444-------name: " + Master.editOrderList.get(position).getName() + " Quantity: " +  Master.editOrderList.get(position).getQuantity());
                                Log.e("cart adapter", "in changeCheck if");
                            }
                            else
                            {
                                total= String.format("%.2f", Master.editOrderList.get(position).getUnitPrice() * Integer.parseInt(editable.toString()));
                                Master.editOrderList.get(position).setQuantity(Integer.parseInt(editable.toString()));
                                System.out.println("555555555-------name: " + Master.editOrderList.get(position).getName() + " Quantity: " +  Master.editOrderList.get(position).getQuantity());
                              //  b.putInt(Master.editOrderList.get(position).getName(), Integer.parseInt(editable.toString()));
                                bPlus.setEnabled(true);
                                bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                            }

                            Master.editOrderList.get(position).setTotal(Double.parseDouble(total));

                            itotal.setText("\u20B9" + Master.editOrderList.get(position).getTotal());
                           // System.out.println(Master.editOrderList.get(position).getName() + "---------------------" + total);
                            sum=0.0;
                            for(int i=0;i<Master.editOrderList.size();i++)
                            {
                                sum=sum+Master.editOrderList.get(i).getTotal();
                               // System.out.println("updating---------------------"+sum);
                            }

                            cartTotal.setText("" + String.format("%.2f",sum));

                        }
                        catch (NumberFormatException e) {
                        }
                    }
                    else
                    {
                      //  fromTextWatchr = true;
                      //  eqty.setText("1");
                        Master.editOrderList.get(position).setQuantity(0);
                        Master.editOrderList.get(position).setTotal(0.0);

                        itotal.setText("\u20B9" + Master.editOrderList.get(position).getTotal());
                        // System.out.println(Master.editOrderList.get(position).getName() + "---------------------" + total);
                        sum=0.0;
                        for(int i=0;i<Master.editOrderList.size();i++)
                        {
                            sum=sum+Master.editOrderList.get(i).getTotal();
                            // System.out.println("updating---------------------"+sum);
                        }

                        cartTotal.setText("" + String.format("%.2f",sum));

                       // b.putInt(Master.editOrderList.get(position).getName(), 1);
                        bMinus.setEnabled(false);
                        bMinus.setColorFilter(context.getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN);

                        bPlus.setEnabled(true);
                        bPlus.setColorFilter(context.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
                    }
                /*}
                else
                {
                    fromTextWatchr = false;
                }*/
            }
            catch (Exception e)
            {

            }

        }
    }

}